String basePath = 'dsljobs'
String dslmaventestPath = 'dblore-jenkins-dsl-maven-test'
String dslgotestPath = 'dblore-jenkins-dsl-go-test'

def dslmavenGitUrl = 'https://gitlab.com/dblore/jenkins-dsl-maven.git'
def dslgoGitUrl = 'https://gitlab.com/dblore/jenkins-dsl-go.git'

folder(basePath) {
  description 'DSL generated folder.'
}

pipelineJob("$basePath/$dslmaventestPath") {
  description()
  definition {
    cpsScm {
      scm {
        git {
          remote {
            url("${dslmavenGitUrl}")
          }
          branch("master")
        }
      }
    triggers {
      gitlabPush {
          buildOnPushEvents(true)
      }
    }
      scriptPath("Jenkinsfile")
    }
  }
}

pipelineJob("$basePath/$dslgotestPath") {
  description()
  definition {
    cpsScm {
      scm {
        git {
          remote {
            url("${dslgoGitUrl}")
          }
          branch("master")
        }
      }
      scriptPath("Jenkinsfile")
    }
  }
}